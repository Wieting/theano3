python gouda_tti-wordonly-reg.py reg-word-model "../data/paragram_sl999.txt" "simlex" > word_commands_gouda_reg.txt

python gouda_tti-sl-wordonly-reg.py reg-word-sl-model "../data/paragram_sl999.txt" "simlex" > slword_commands_gouda_reg.txt

python gouda_tti-glove-wordonly-reg.py reg-word-glove-model "../data/paragram_sl999.txt" "simlex" > gloveword_commands_gouda_reg.txt