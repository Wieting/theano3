#!/bin/bash

#SBATCH --partition=contrib-gpu-long --cpus-per-task=1 --constraint=5g --array=1-4
bash -c "`sed "${SLURM_ARRAY_TASK_ID}q;d" anno_cmds.txt`"
