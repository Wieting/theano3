from glob import glob

best_word = "command: sh train_gouda.sh -wordstem simlex -wordfile /share/data/speech/Data/jwieting/paraphrase_embeddings/tuning/experiment1/training_phrases/../data/paragram_sl999.txt -outfile word-model -updatewords True -hiddensize 300 -dataf /share/data/speech/Data/jwieting/paraphrase_embeddings/tuning/experiment1/training_phrases/../data/data-plot/500k_xxl_tuning.txt -layersize 300 -save False -nntype word -evaluate True -epochs 5 -batchsize 50 -LW 1e-08 -margin 0.4 -samplingtype MAX"
best_proj = "command: sh train_gouda.sh -wordstem simlex -wordfile /share/data/speech/Data/jwieting/paraphrase_embeddings/tuning/experiment1/training_phrases/../data/paragram_sl999.txt -outfile proj-model -hiddensize 300 -dataf /share/data/speech/Data/jwieting/paraphrase_embeddings/tuning/experiment1/training_phrases/../data/data-plot/500k_xxl_tuning.txt -layersize 300 -save False -nntype proj -evaluate True -epochs 5 -nonlinearity 1 -updatewords True -batchsize 100 -LW 1e-07 -margin 0.4 -samplingtype MAX -LC 1e-06"
best_lstm = "command: sh train_gouda.sh -wordstem simlex -wordfile /share/data/speech/Data/jwieting/paraphrase_embeddings/tuning/experiment1/training_phrases/../data/paragram_sl999.txt -outfile lstm-model -hiddensize 300 -dataf /share/data/speech/Data/jwieting/paraphrase_embeddings/tuning/experiment1/training_phrases/../data/data-plot/500k_xxl_tuning.txt -layersize 300 -save False -nntype lstm -peephole True -evaluate True -epochs 5 -outgate True -updatewords True -batchsize 50 -LW 1e-08 -margin 0.4 -samplingtype MIX -LC 0.0001"

cmd1_cpu = "sh train1.sh -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile word-model -updatewords True -hiddensize 300 -dataf ../data/data-time-plot/100k_xxl_timing.txt -layersize 300 -save False -nntype word -evaluate False -epochs 1 -batchsize 50 -LW 1e-08 -margin 0.4 -samplingtype MAX > cpu_word"
cmd2_cpu = "sh train1.sh -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile proj-model -hiddensize 300 -dataf ../data/data-time-plot/100k_xxl_timing.txt -layersize 300 -save False -nntype proj -evaluate False -epochs 1 -nonlinearity 1 -updatewords True -batchsize 100 -LW 1e-07 -margin 0.4 -samplingtype MAX -LC 1e-06 > cpu_proj"
cmd3_cpu = "sh train1.sh -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile lstm-model -hiddensize 300 -dataf ../data/data-time-plot/100k_xxl_timing.txt -layersize 300 -save False -nntype lstm -peephole True -evaluate False -epochs 1 -outgate True -updatewords True -batchsize 50 -LW 1e-08 -margin 0.4 -samplingtype MIX -LC 0.0001 > cpu_lstm"

cmd1_gpu = "sh train_gpu.sh -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile word-model -updatewords True -hiddensize 300 -dataf ../data/data-time-plot/100k_xxl_timing.txt -layersize 300 -save False -nntype word -evaluate False -epochs 1 -batchsize 50 -LW 1e-08 -margin 0.4 -samplingtype MAX > gpu_word"
cmd2_gpu = "sh train_gpu.sh -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile proj-model -hiddensize 300 -dataf ../data/data-time-plot/100k_xxl_timing.txt -layersize 300 -save False -nntype proj -evaluate False -epochs 1 -nonlinearity 1 -updatewords True -batchsize 100 -LW 1e-07 -margin 0.4 -samplingtype MAX -LC 1e-06 > gpu_proj"
cmd3_gpu = "sh train_gpu.sh -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile lstm-model -hiddensize 300 -dataf ../data/data-time-plot/100k_xxl_timing.txt -layersize 300 -save False -nntype lstm -peephole True -evaluate False -epochs 1 -outgate True -updatewords True -batchsize 50 -LW 1e-08 -margin 0.4 -samplingtype MIX -LC 0.0001 > gpu_lstm"

print cmd1_cpu
print cmd2_cpu
print cmd3_cpu
print cmd1_gpu
print cmd2_gpu
print cmd3_gpu