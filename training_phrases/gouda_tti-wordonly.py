import sys
import os

fname = sys.argv[1]
wordfile = sys.argv[2]
wordstem = sys.argv[3]
dim = 300
data = '../data/data-time-plot/ppdb-100k.txt'

LW = [1E-5,1E-6,1E-7,1E-8]
batchsize = [25,50,100]
margins = [0.4, 0.6, 0.8]
samplingtypes = ['MAX','MIX']
learner = ['adagrad','adam']
eta = [0.05,0.005,0.0005]
clip = [1,0]


cmd = "sh train_gouda.sh -wordstem {0} -wordfile {1} -outfile {2} \
-updatewords True -hiddensize 300 -dataf {3} -layersize {4} \
-save False -nntype word -evaluate True -epochs 5".format(wordstem,wordfile,fname,data,dim)

for i in batchsize:
    for j in LW:
        for k in margins:
            for l in samplingtypes:
                for clp in clip:
                    for et in eta:
                        for lrn in learner:
                            next_cmd = cmd + " -batchsize {0} -LW {1} -margin {2} -samplingtype {3} -clip {4} -eta {5} -learner {6}".format(i,j,k,l,clp,et,lrn)
                            print next_cmd
