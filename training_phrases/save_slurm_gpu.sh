#!/bin/bash

#SBATCH --partition=contrib-gpu-long --cpus-per-task=1 --array=1-1
bash -c "`sed "${SLURM_ARRAY_TASK_ID}q;d" save_cmds.txt`"
