from glob import glob

xl_data = "../data/data-time-plot/xl/xldata_11850.txt\t \
../data/data-time-plot/xl/xldata_11850_rand.txt\t \
../data/data-time-plot/xl/xldata_1481.txt\t \
../data/data-time-plot/xl/xldata_1481_rand.txt\t \
../data/data-time-plot/xl/xldata_1516876.txt\t \
../data/data-time-plot/xl/xldata_1516876_rand.txt\t \
../data/data-time-plot/xl/xldata_189609.txt\t \
../data/data-time-plot/xl/xldata_189609_rand.txt\t \
../data/data-time-plot/xl/xldata_23701.txt\t \
../data/data-time-plot/xl/xldata_23701_rand.txt\t \
../data/data-time-plot/xl/xldata_2962.txt\t \
../data/data-time-plot/xl/xldata_2962_rand.txt\t \
../data/data-time-plot/xl/xldata_3033753.txt\t \
../data/data-time-plot/xl/xldata_3033753_rand.txt\t \
../data/data-time-plot/xl/xldata_379219.txt\t \
../data/data-time-plot/xl/xldata_379219_rand.txt\t \
../data/data-time-plot/xl/xldata_47402.txt\t \
../data/data-time-plot/xl/xldata_47402_rand.txt\t \
../data/data-time-plot/xl/xldata_5925.txt\t \
../data/data-time-plot/xl/xldata_5925_rand.txt\t \
../data/data-time-plot/xl/xldata_740.txt\t \
../data/data-time-plot/xl/xldata_740_rand.txt\t \
../data/data-time-plot/xl/xldata_758438.txt\t \
../data/data-time-plot/xl/xldata_758438_rand.txt\t \
../data/data-time-plot/xl/xldata_94804.txt\t \
../data/data-time-plot/xl/xldata_94804_rand.txt"

xxl_data = "../data/data-time-plot/xxl/xxldata_1113.txt\t \
../data/data-time-plot/xxl/xxldata_1113_rand.txt\t \
../data/data-time-plot/xxl/xxldata_1140446.txt\t \
../data/data-time-plot/xxl/xxldata_1140446_rand.txt\t \
../data/data-time-plot/xxl/xxldata_142555.txt\t \
../data/data-time-plot/xxl/xxldata_142555_rand.txt\t \
../data/data-time-plot/xxl/xxldata_17819.txt\t \
../data/data-time-plot/xxl/xxldata_17819_rand.txt\t \
../data/data-time-plot/xxl/xxldata_2227.txt\t \
../data/data-time-plot/xxl/xxldata_2227_rand.txt\t \
../data/data-time-plot/xxl/xxldata_2280893.txt\t \
../data/data-time-plot/xxl/xxldata_2280893_rand.txt\t \
../data/data-time-plot/xxl/xxldata_285111.txt\t \
../data/data-time-plot/xxl/xxldata_285111_rand.txt\t \
../data/data-time-plot/xxl/xxldata_35638.txt\t \
../data/data-time-plot/xxl/xxldata_35638_rand.txt\t \
../data/data-time-plot/xxl/xxldata_4454.txt\t \
../data/data-time-plot/xxl/xxldata_4454_rand.txt\t \
../data/data-time-plot/xxl/xxldata_4561787.txt\t \
../data/data-time-plot/xxl/xxldata_4561787_rand.txt\t \
../data/data-time-plot/xxl/xxldata_556.txt\t \
../data/data-time-plot/xxl/xxldata_556_rand.txt\t \
../data/data-time-plot/xxl/xxldata_570223.txt\t \
../data/data-time-plot/xxl/xxldata_570223_rand.txt\t \
../data/data-time-plot/xxl/xxldata_71277.txt\t \
../data/data-time-plot/xxl/xxldata_71277_rand.txt\t \
../data/data-time-plot/xxl/xxldata_8909.txt\t \
../data/data-time-plot/xxl/xxldata_8909_rand.txt\t \
../data/data-time-plot/xxl/xxldata_9123575.txt\t \
../data/data-time-plot/xxl/xxldata_9123575_rand.txt"

best_word = "command: sh train_gouda.sh -wordstem simlex -wordfile /share/data/speech/Data/jwieting/paraphrase_embeddings/tuning/experiment1/training_phrases/../data/paragram_sl999.txt -outfile word-model -updatewords True -hiddensize 300 -dataf /share/data/speech/Data/jwieting/paraphrase_embeddings/tuning/experiment1/training_phrases/../data/data-plot/500k_xxl_tuning.txt -layersize 300 -save False -nntype word -evaluate True -epochs 5 -batchsize 50 -LW 1e-08 -margin 0.4 -samplingtype MAX"
best_proj = "command: sh train_gouda.sh -wordstem simlex -wordfile /share/data/speech/Data/jwieting/paraphrase_embeddings/tuning/experiment1/training_phrases/../data/paragram_sl999.txt -outfile proj-model -hiddensize 300 -dataf /share/data/speech/Data/jwieting/paraphrase_embeddings/tuning/experiment1/training_phrases/../data/data-plot/500k_xxl_tuning.txt -layersize 300 -save False -nntype proj -evaluate True -epochs 5 -nonlinearity 1 -updatewords True -batchsize 100 -LW 1e-07 -margin 0.4 -samplingtype MAX -LC 1e-06"
best_lstm = ""

cmd_word = "sh train_gpu.sh -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile word-model -updatewords True -hiddensize 300 -dataf {0} -layersize 300 -save False -nntype word -evaluate True -epochs 10 -batchsize 100 -LW 1e-07 -margin 0.4 -samplingtype MIX"
#cmd_proj = "sh train_gpu.sh -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile proj-model -hiddensize 300 -dataf {0} -layersize 300 -save False -nntype proj -evaluate True -epochs 10 -nonlinearity 1 -updatewords True -batchsize 100 -LW 1e-07 -margin 0.4 -samplingtype MAX -LC 1e-06"
#cmd_lstm = "sh train_gpu.sh -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile lstm-model -hiddensize 300 -dataf {0} -layersize 300 -save False -nntype lstm -peephole True -evaluate True -epochs 10 -outgate True -updatewords True -batchsize 50 -LW 1e-08 -margin 0.4 -samplingtype MIX -LC 0.0001"
#cmd_lstm_nooutputgate = "sh train_gpu.sh -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile lstm-model -hiddensize 300 -dataf {0} -layersize 300 -save False -nntype lstm -peephole True -evaluate True -epochs 10 -outgate False -updatewords True -batchsize 100 -LW 1e-08 -margin 0.4 -samplingtype MIX -LC 1e-05"

xl_f = xl_data.split('\t')
xxl_f = xxl_data.split('\t')

fout_word = open('plot_word.txt','w')
#fout_proj = open('plot_proj.txt','w')
#fout_lstm = open('plot_lstm.txt','w')
#fout_lstm_nooutgate = open('plot_lstm_nooutputgate.txt','w')
for i in xl_f:
    fout_word.write(cmd_word.format(i.strip())+'\n')
    #fout_proj.write(cmd_proj.format(i.strip())+'\n')
    #fout_lstm.write(cmd_lstm.format(i.strip())+'\n')
    #fout_lstm_nooutgate.write(cmd_lstm_nooutputgate.format(i.strip())+'\n')
#for i in xxl_f:
#    print cmd_word.format(i.strip())
#    print cmd_proj.format(i.strip())
