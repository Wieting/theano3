

lstm_jhu = "sh train_gpu.sh -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile lstm-model -hiddensize 300 -dataf ../data/data-time-plot/ppdb-XL-ordered-data.txt -layersize 300 -save True -nntype lstm -peephole True -evaluate True -epochs 10 -outgate True -updatewords True -batchsize 50 -LW 1e-07 -margin 0.4 -samplingtype MIX -LC 0.0001"

lstm_jhu_nooutgate = "sh train_gpu.sh -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile lstm-model -hiddensize 300 -dataf ../data/data-time-plot/ppdb-XL-ordered-data.txt -layersize 300 -save True -nntype lstm -peephole True -evaluate True -epochs 10 -outgate False -updatewords True -batchsize 50 -LW 1e-08 -margin 0.4 -samplingtype MIX -LC 1e-06"

word_jhu = "sh train_gpu.sh -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile word-model -updatewords True -hiddensize 300 -dataf ../data/data-time-plot/ppdb-XL-ordered-data.txt -layersize 300 -save True -nntype word -evaluate True -epochs 10 -batchsize 100 -LW 1e-08 -margin 0.4 -samplingtype MAX"

proj_jhu = "sh train_gpu.sh -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile proj-model -hiddensize 300 -dataf ../data/data-time-plot/ppdb-XL-ordered-data.txt -layersize 300 -save True -nntype proj -evaluate True -epochs 10 -nonlinearity 1 -updatewords True -batchsize 100 -LW 1e-06 -margin 0.4 -samplingtype MIX -LC 1e-06"


word_anno = "sh train_gpu.sh -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile word-model -updatewords True -hiddensize 300 -dataf ../data/data-time-plot/ppdb-XL-ordered-data.txt -layersize 300 -save True -nntype word -evaluate True -epochs 10 -batchsize 100 -LW 1e-07 -margin 0.4 -samplingtype MIX"

proj_anno = "sh train_gpu.sh -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile proj-model -hiddensize 300 -dataf ../data/data-time-plot/ppdb-XL-ordered-data.txt -layersize 300 -save True -nntype proj -evaluate True -epochs 10 -nonlinearity 1 -updatewords True -batchsize 100 -LW 1e-06 -margin 0.4 -samplingtype MIX -LC 0.0001"

lstm_anno = "sh train_gpu.sh -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile lstm-model -hiddensize 300 -dataf ../data/data-time-plot/ppdb-XL-ordered-data.txt -layersize 300 -save True -nntype lstm -peephole True -evaluate True -epochs 10 -outgate True -updatewords True -batchsize 100 -LW 1e-07 -margin 0.4 -samplingtype MAX -LC 0.0001"

lstm_anno_nooutgate = "sh train_gpu.sh -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile lstm-model -hiddensize 300 -dataf ../data/data-time-plot/ppdb-XL-ordered-data.txt -layersize 300 -save True -nntype lstm -peephole True -evaluate True -epochs 10 -outgate False -updatewords True -batchsize 100 -LW 1e-06 -margin 0.4 -samplingtype MIX -LC 0.0001"

print word_anno
print proj_anno
print lstm_anno
print lstm_anno_nooutgate

#print lstm_jhu_nooutgate
#print lstm_jhu
#print word_jhu
#print proj_jhu
