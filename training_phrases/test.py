import numpy as np
import theano
from theano import tensor as T
from theano import config

x = np.random.rand(2,3,5)

m = np.eye(5)

print x

print m

np.dot(x,m)

print np.dot(x,2*m)

t1 = T.tensor3()
m1 = T.matrix()

f = theano.function([t1,m1],T.dot(t1,m1))

print f(x,2*m)