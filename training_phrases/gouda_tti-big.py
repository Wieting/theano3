import sys
import os

fname = sys.argv[1]
wordfile = sys.argv[2]
wordstem = sys.argv[3]
dim = 300
data = '../data/data-time-plot/ppdb-100k.txt'

LC = [1E-3,1E-4,1E-5,1E-6,1E-7,1E-8,1E-9]
LW = [1E-5,1E-6,1E-7,1E-8]
batchsize = [100]
margins = [0.4]
samplingtypes = ['MAX']
learner = ['adagrad','adam']
eta = [0.05,0.005,0.0005]
clip = [1,0]
big = [1200,2400,4800]

cmd = "sh train_gouda.sh -wordstem {0} -wordfile {1} -outfile {2} \
-hiddensize 300 -dataf {3} -layersize {4} \
-save False -nntype big \
-evaluate True -epochs 5".format(wordstem,wordfile,fname,data,dim)

for i in batchsize:
    for k in margins:
        for l in samplingtypes:
            for m in LC:
                for clp in clip:
                    for et in eta:
                        for lrn in learner:
                            for j in LW:
                                for bi in big:
                                    next_cmd = cmd + " -updatewords True -batchsize {0} -LW {1} -margin {2} -samplingtype {3} -LC {4} -clip {5} -eta {6} -learner {7} -big {8}".format(i,j,k,l,m,clp,et,lrn,bi)
                                    print next_cmd
                                next_cmd = cmd + " -updatewords False -batchsize {0} -margin {1} -samplingtype {2} -LC {3} -clip {4} -eta {5} -learner {6} -big {7}".format(i,k,l,m,clp,et,lrn,bi)
                                print next_cmd