python gouda_tti-wordonly-rep.py rep-word-model "../data/paragram_sl999.txt" "simlex" > word_commands_gouda_rep.txt

python gouda_tti-sl-wordonly-rep.py rep-word-sl-model "../data/paragram_sl999.txt" "simlex" > slword_commands_gouda_rep.txt

python gouda_tti-glove-wordonly-rep.py rep-word-glove-model "../data/paragram_sl999.txt" "simlex" > gloveword_commands_gouda_rep.txt