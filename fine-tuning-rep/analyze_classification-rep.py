from glob import glob

class datapoint(object):
    def __init__(self, dev,tst,model,type,key):
        self.dev = dev
        self.tst = tst
        self.model = model
        self.type = type
        self.key = key

flist = glob('slurm*.out')
datas = []
for i in flist:
    f = open(i,'r')
    lines = f.readlines()
    type = None
    model = None
    bd = 0.
    bt = 0.
    key = None
    for j in lines:
        if 'sickdev' in j:
            type = 'sick'
            key = j
        if 'rep-proj-model' in j:
            model = 'proj'
            key = j
        if 'twitter' in j:
            type = 'twitter'
            key = j
        if 'rep-word-model' in j:
            model = 'word'
            key = j
        if 'rep-word-sl-model' in j:
            model = 'slword'
            key = j
        if 'rep-word-glove-model' in j:
            model = 'gloveword'
            key = j
        if 'rep-lstm-output-model' in j:
            model = 'outputlstm'
            key = j
        if 'rep-lstm-nooutput-model' in j:
            model = 'nooutputlstm'
            key = j
        if 'evaluation:' in j:
            j = j.split()
            d = float(j[1])
            t = float(j[3])
            if d > bd:
                bd = d
                bt = t
    datas.append(datapoint(bd,bt,model,type,key))


word_tw = []
word_si = []
slword_tw = []
slword_si = []
gloveword_tw = []
gloveword_si = []
proj_tw = []
proj_si = []
outputlstm_tw = []
outputlstm_si = []
nooutputlstm_tw = []
nooutputlstm_si = []

for i in datas:
    if i.model == 'word' and i.type == 'twitter':
        word_tw.append(i)
    if i.model == 'word' and i.type == 'sick':
        word_si.append(i)
    if i.model == 'slword' and i.type == 'twitter':
        slword_tw.append(i)
    if i.model == 'slword' and i.type == 'sick':
        slword_si.append(i)
    if i.model == 'gloveword' and i.type == 'twitter':
        gloveword_tw.append(i)
    if i.model == 'gloveword' and i.type == 'sick':
        gloveword_si.append(i)
    if i.model == 'proj' and i.type == 'twitter':
        proj_tw.append(i)
    if i.model == 'proj' and i.type == 'sick':
        proj_si.append(i)
    if i.model == 'outputlstm' and i.type == 'twitter':
        outputlstm_tw.append(i)
    if i.model == 'outputlstm' and i.type == 'sick':
        outputlstm_si.append(i)
    if i.model == 'nooutputlstm' and i.type == 'twitter':
        nooutputlstm_tw.append(i)
    if i.model == 'nooutputlstm' and i.type == 'sick':
        nooutputlstm_si.append(i)

lis = []
lis.append(word_tw)
lis.append(word_si)
lis.append(slword_tw)
lis.append(slword_si)
lis.append(gloveword_tw)
lis.append(gloveword_si)
lis.append(proj_tw)
lis.append(proj_si)
lis.append(outputlstm_tw)
lis.append(outputlstm_si)
lis.append(nooutputlstm_tw)
lis.append(nooutputlstm_si)

for i in lis:
    i.sort(key=lambda x: x.dev, reverse=True)
    print len(i)
    if len(i) > 0:
        print i[0].model, i[0].dev, i[0].tst, i[0].key