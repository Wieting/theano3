from scipy.spatial.distance import cosine
from scipy.stats import spearmanr
from scipy.stats import pearsonr
from utils import lookupIDX
import utils
import sys
import numpy as np

def getAddScore(p1,p2,We,words):
    p1 = p1.split()
    p2 = p2.split()
    X1 = []
    X2 = []
    for i in p1:
        X1.append(utils.lookup(We,words,i))
    for i in p2:
        X2.append(utils.lookup(We,words,i))
    #pdb.set_trace()
    em1 =  np.matrix(X1).sum(axis=0)/len(p1)
    em2 =  np.matrix(X2).sum(axis=0)/len(p2)
    return -cosine(em1,em2)+1

def getAddCorrelation(We,words,f):
    f = open(f,'r')
    lines = f.readlines()
    preds = []
    golds = []
    for i in lines:
        i = i.split("\t")
        p1 = i[0]; p2 = i[1]; score = float(i[2])
        pred = getAddScore(p1,p2,We,words)
        preds.append(pred)
        golds.append(score)
    return pearsonr(preds,golds)[0], spearmanr(preds,golds)[0]

def evaluate_add(We,words):
    prefix = "../datasets_tokenized/"
    parr = []; sarr = []; farr = []

    farr = ["FNWN2013",
            "JHUppdb",
            "MSRpar2012",
            "MSRvid2012",
            "OnWN2012",
            "OnWN2013",
            "OnWN2014",
            "SMT2013",
            "SMTeuro2012",
            "SMTnews2012",
            "anno-dev",
            "anno-test",
            "answer-forum2015",
            "answer-student2015",
            "belief2015",
            "bigram-jn",
            "bigram-nn",
            "bigram-vn",
            "deft-forum2014",
            "deft-news2014",
            "headline2013",
            "headline2014",
            "headline2015",
            "images2014",
            "images2015",
            "sicktest",
            "tweet-news2014",
            "twitter"]

    for i in farr:
        p,s = getAddCorrelation(We,words,prefix+i)
        parr.append(p); sarr.append(s)

    s = ""
    for i,j,k in zip(parr,sarr,farr):
        s += str(i)+" "+str(j)+" "+k+" | "

    #get means for 2012, 2013, 2014, 2015
    n = parr[2]+ parr[3]+parr[4]+parr[8]+parr[9]
    n = n/5.
    s += str(n)+" 2012-average | "

    n = parr[0]+ parr[5]+parr[7]+parr[20]
    n = n/4.
    s += str(n)+" 2013-average | "

    n = parr[6]+ parr[18]+parr[19]+parr[21]+parr[23]+parr[26]
    n = n/6.
    s += str(n)+" 2014-average | "

    n = parr[12]+ parr[13]+parr[14]+parr[22]+parr[24]
    n = n/5.
    s += str(n)+" 2015-average | "

    print s

if __name__ == "__main__":
    wordfile = sys.argv[1]
    (words, We) = utils.getWordmap(wordfile)
    evaluate_add(We,words)
