class params(object):
    def __init__(self, LW=1E-5, LC=0.0001, LRC=1E-3, dataf='../data/phrase_pairs', clip=False,
                 batchsize=100, margin=1, epochs=5, eta=0.050, evaluate=True, save=False, memsize=50,
                 hiddensize=25, outfile="test.out", type="MAX", wordfile='../data/skipwiki25.txt'):
        self.LW = LW
        self.LC = LC
        self.LRC = LRC
        self.dataf = dataf
        self.batchsize = batchsize
        self.margin = margin
        self.epochs = epochs
        self.eta = eta
        self.evaluate = evaluate
        self.save = save
        self.data = []
        self.hiddensize = hiddensize
        self.outfile = outfile
        self.type = type
        self.wordfile = wordfile
        self.clip = clip
        self.memsize = memsize

    def __str__(self):
        t = "LW", self.LW, ", LC", self.LC
        t = map(str, t)
        return ' '.join(t)