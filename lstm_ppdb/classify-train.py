from utils import getWordmap
from params import params
from utils import getDataDataset
from classification_lstm_ppdb_model import classification_lstm_ppdb_model
from classification_word_model import classification_word_model
from classification_linear_projection_model import classification_linear_projection_model
import lasagne
import random
import numpy as np
import sys
import argparse


def str2bool(v):
    if v is None:
        return False
    if v.lower() in ("yes", "true", "t", "1"):
        return True
    if v.lower() in ("no", "false", "f", "0"):
        return False
    raise ValueError('A type that was supposed to be boolean is not boolean.')


def learner2bool(v):
    if v is None:
        return lasagne.updates.adagrad
    if v.lower() == "adagrad":
        return lasagne.updates.adagrad
    if v.lower() == "adam":
        return lasagne.updates.adam
    raise ValueError('A type that was supposed to be a learner is not.')


random.seed(1)
np.random.seed(1)

params = params()

parser = argparse.ArgumentParser()
parser.add_argument("-LW", help="Regularization on Words", type=float)
parser.add_argument("-LC", help="Regularization on Composition Parameters", type=float)
parser.add_argument("-outfile", help="Output file name")
parser.add_argument("-batchsize", help="Size of batch", type=int)
parser.add_argument("-hiddensize", help="Size of input", type=int)
parser.add_argument("-memsize", help="Size of Memory layer. Only used when trained for classification.",
                    type=int)
parser.add_argument("-wordfile", help="Word file to be read in.")
parser.add_argument("-layersize", help="Size of Output layer.", type=int)
parser.add_argument("-updatewords", help="Whether to update the word embeddings")
parser.add_argument("-wordstem", help="Nickname of word embeddings used.")
parser.add_argument("-save", help="Whether to pickle the model.")
parser.add_argument("-traindata", help="Training data file.")
parser.add_argument("-devdata", help="Training data file.")
parser.add_argument("-testdata", help="Testing data file.")
parser.add_argument("-margin", help="Regularization on Words", type=float)
parser.add_argument("-samplingtype", help="Type of Sampling used.")
parser.add_argument("-peephole", help="Whether to use peepholes in LSTM.")
parser.add_argument("-outgate", help="Whether to use output gate in LSTM.")
parser.add_argument("-nonlinearity", help="Type of nonlinearity in projection model.",
                    type=int)
parser.add_argument("-nntype", help="Type of neural network.")
parser.add_argument("-evaluate", help="Whether to evaluate the model during training.")
parser.add_argument("-epochs", help="Number of epochs in training.", type=int)
parser.add_argument("-regfile", help="Path to model file that we want to regularize towards.")
parser.add_argument("-minval", help="min rating possible in scoring.", type=int)
parser.add_argument("-maxval", help="max rating in scoring.", type=int)
parser.add_argument("-LRW", help="lambda for regularization word parameters", type=float)
parser.add_argument("-LRC", help="lambda for regularization composition parameters", type=float)
parser.add_argument("-usereps", help="Do not update representations")
parser.add_argument("-clip", help="float to indicate grad cutting or False")
parser.add_argument("-eta", help="learning rate")
parser.add_argument("-learner", help="Either AdaGrad or Adam")

args = parser.parse_args()

params.LW = args.LW
params.LC = args.LC
params.outfile = args.outfile
params.batchsize = args.batchsize
params.hiddensize = args.hiddensize
params.memsize = args.memsize
params.wordfile = args.wordfile
params.nntype = args.nntype
params.layersize = args.layersize
params.updatewords = str2bool(args.updatewords)
params.wordstem = args.wordstem
params.save = str2bool(args.save)
params.traindata = args.traindata
params.devdata = args.devdata
params.testdata = args.testdata
params.margin = args.margin
params.samplingtype = args.samplingtype
params.peephole = str2bool(args.peephole)
params.outgate = str2bool(args.outgate)
params.nntype = args.nntype
params.epochs = args.epochs
params.evaluate = str2bool(args.evaluate)
params.LRW = args.LRW
params.LRC = args.LRC
params.learner = learner2bool(args.learner)

if args.eta:
    params.eta = args.eta

if args.clip:
    params.clip = args.clip

reg_file = args.regfile
minval = args.minval
maxval = args.maxval
usereps = str2bool(args.usereps)

if args.nonlinearity:
    if args.nonlinearity == 1:
        params.nonlinearity = lasagne.nonlinearities.linear
    if args.nonlinearity == 2:
        params.nonlinearity = lasagne.nonlinearities.tanh
    if args.nonlinearity == 3:
        params.nonlinearity = lasagne.nonlinearities.rectify
    if args.nonlinearity == 4:
        params.nonlinearity = lasagne.nonlinearities.sigmoid

(words, We) = getWordmap(params.wordfile)
wordfilestem = params.wordfile.split("/")[-1].replace(".txt", "")
params.outfile = "../models/" + params.outfile + "." + str(params.LW) + "." + str(params.LC) + "." + \
                 str(params.batchsize) + "." + params.samplingtype + "." + str(params.margin) + "." + \
                 str(params.hiddensize) + "." + params.wordstem + ".txt." + params.nntype

if args.nonlinearity:
    params.outfile = "../models/" + params.outfile + "." + str(params.LW) + "." + str(params.LC) + "." + \
                     str(params.batchsize) + "." + params.samplingtype + "." + str(params.margin) + "." + \
                     str(params.hiddensize) + "." + params.wordstem + ".txt." + params.nntype + "." + \
                     str(args.nonlinearity)

train_data = getDataDataset(params.traindata, words)
#dev_data = getDataDataset(params.devdata,words)
#test_data = getDataDataset(params.testdata,words)

print "Saving to: " + params.outfile

model = None

print sys.argv

if params.nntype == 'lstm':
    model = classification_lstm_ppdb_model(We, minval, maxval, reg_file, usereps, params)
elif params.nntype == 'word':
    model = classification_word_model(We, minval, maxval, reg_file, usereps, params)
elif params.nntype == 'proj':
    model = classification_linear_projection_model(We, minval, maxval, reg_file, usereps, params)
else:
    "Error no type specified"

model.train(train_data, params.devdata, params.testdata, params.traindata, words, params)