import theano
import numpy as np
from theano import tensor as T
from theano import config
import pdb
from evaluate import evaluate_all
import time
import lasagne
from AverageLayer import AverageLayer
import sys
import cPickle
from evaluate import evaluate

p1 = cPickle.load(open('../data/word-sl-pickle.pickle','rb'))
p2 = cPickle.load(open('../data/word-jhu-pickle.pickle','rb'))

W1 = p1[0].get_value()
W2 = p2[0].get_value()

x = (W1-W2)**2
print x.sum()