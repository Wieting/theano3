

#test userep for word
python classify-train.py -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile rep-word-model -updatewords True -hiddensize 300 -traindata ../datasets_tokenized/sicktrain -devdata ../datasets_tokenized/sickdev -testdata ../datasets_tokenized/sicktest -layersize 300 -save False -nntype word -evaluate True -epochs 20 -minval 1 -maxval 5 -usereps True -regfile ../data/word-pickle -batchsize 100 -LC 1e-05

#test usereg for word
-wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile reg-word-model -updatewords True -hiddensize 300 -traindata ../datasets_tokenized/sicktrain -devdata ../datasets_tokenized/sickdev -testdata ../datasets_tokenized/sicktest -layersize 300 -save False -nntype word -evaluate True -epochs 20 -minval 1 -maxval 5 -usereps False -regfile ../data/word-pickle -batchsize 100 -LR 1e-05 -LC 1e-05

#test userep for projection
python classify-train.py -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile proj-model -updatewords True -hiddensize 300 -traindata ../datasets_tokenized/sicktrain -devdata ../datasets_tokenized/sickdev -testdata ../datasets_tokenized/sicktest -layersize 300 -save False -nntype proj -evaluate True -epochs 20 -nonlinearity 1 -minval 1 -maxval 5 -usereps True -regfile ../data/proj-pickle -batchsize 100 -LW 1e-05 -LC 1e-05

#test usereg for projection
THEANO_FLAGS=mode=FAST_RUN,device=cpu,floatX=float32 python classify-train.py -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile proj-model -updatewords True -hiddensize 300 -traindata ../datasets_tokenized/sicktrain -devdata ../datasets_tokenized/sickdev -testdata ../datasets_tokenized/sicktest -layersize 300 -save False -nntype proj -evaluate True -epochs 20 -nonlinearity 1 -minval 1 -maxval 5 -usereps False -regfile ../data/proj-jhu-pickle.pickle -batchsize 50 -LC 1e-05 -LRW 1E-8 -memsize 150 -LRC 1E-8

#twitter classification - word
python classify-train.py -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile word-model -updatewords True -hiddensize 300 -traindata ../datasets_tokenized/twitter-train -devdata ../datasets_tokenized/twitter-dev -testdata ../datasets_tokenized/twitter -layersize 300 -save False -nntype word -evaluate True -epochs 20 -nonlinearity 1 -minval 1 -maxval 2 -batchsize 250 -LW 1e-07 -LC 1e-08

#32 bit - word (can swap for proj or lstm)
THEANO_FLAGS=mode=FAST_RUN,device=cpu,floatX=float32 python classify-train.py -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile word-model -updatewords True -hiddensize 300 -traindata ../datasets_tokenized/twitter-train -devdata ../datasets_tokenized/twitter-dev -testdata ../datasets_tokenized/twitter -layersize 300 -save False -nntype word -evaluate True -epochs 20 -nonlinearity 1 -minval 1 -maxval 2 -batchsize 250 -LW 1e-07 -LC 1e-08

#32 bit - word - ppdb
THEANO_FLAGS=mode=FAST_RUN,device=cpu,floatX=float32 python train.py -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile word-model -updatewords True -hiddensize 300 -dataf ../data/data-plot/xxldata9.txt -layersize 300 -save False -nntype word -evaluate False -epochs 5 -batchsize 100 -LW 1e-05 -margin 0.4 -samplingtype MAX

#32 bit - proj - ppdb
THEANO_FLAGS=mode=FAST_RUN,device=cpu,floatX=float32 python train.py -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile word-model -updatewords True -hiddensize 300 -dataf ../data/data-plot/xxldata9.txt -layersize 300 -save False -nntype proj -evaluate False -epochs 5 -batchsize 100 -LW 1e-05 -margin 0.4 -samplingtype MAX -nonlinearity 1 -LC 1E-6

#test usereg for lstm
python classify-train.py -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile proj-model -updatewords True -hiddensize 300 -traindata ../datasets_tokenized/sicktrain -devdata ../datasets_tokenized/sickdev -testdata ../datasets_tokenized/sicktest -layersize 300 -save False -nntype lstm -evaluate True -epochs 20 -nonlinearity 1 -minval 1 -maxval 5 -usereps False -regfile ../data/lstm-pickle -peephole True -batchsize 100 -LW 1e-05 -LC 1e-05 -LRW 1E-5 -LRC 1E-5

#test usereps with lstm
python classify-train.py -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile proj-model -updatewords True -hiddensize 300 -traindata ../datasets_tokenized/sicktrain -devdata ../datasets_tokenized/sickdev -testdata ../datasets_tokenized/sicktest -layersize 300 -save False -nntype lstm -evaluate True -epochs 20 -nonlinearity 1 -minval 1 -maxval 5 -usereps True -regfile ../data/lstm-pickle -peephole True -batchsize 100 -LW 1e-05 -LC 1e-05 -LRW 1E-5 -LRC 1E-5

#wt-word
THEANO_FLAGS=mode=FAST_RUN,device=cpu,floatX=float32 python train.py -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile word-model -updatewords True -hiddensize 300 -dataf ../data/data-time-plot/500k_xxl_tuning.txt -layersize 300 -save False -nntype wtword -evaluate False -epochs 5 -batchsize 100 -LC 1e-02 -LW 1e-05 -margin 0.4 -samplingtype MAX
