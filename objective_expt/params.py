
class params(object):

    def __init__(self, eta = 0.050):
        self.eta = eta

    def __str__(self):
     t = "LW", self.LW, ", LC", self.LC, ", dataf", self.dataf, ", batchsize", self.batchsize, \
         ", margin",self.margin, ", epochs", self.epochs, ", eta", self.eta, ", evaluate",self.evaluate, \
         ", save", self.save, ", hiddensize", self.hiddensize, ", outfile", self.outfile, ", type", \
         self.type, ", wordfile", self.wordfile
     t = map(str, t)
     return ' '.join(t)