from utils import getWordmap
from params import params
from utils import getData
from lstm_ppdb_model import lstm_ppdb_model
from word_model import word_model
from linear_projection_model import linear_projection_model
import lasagne
import random
import numpy as np
import sys
import argparse

def str2bool(v):
  if v == None:
    return False
  if v.lower() in ("yes", "true", "t", "1"):
    return True
  if v.lower() in ("no", "false", "f", "0"):
    return False
  raise ValueError('A type that was supposed to be boolean is not boolean.')


random.seed(1)
np.random.seed(1)

params = params()

parser = argparse.ArgumentParser()
parser.add_argument("-batchsize", help="Size of batch", type=int)
parser.add_argument("-hiddensize", help="Size of input", type=int)
parser.add_argument("-wordfile", help="Word file to be read in.")
parser.add_argument("-layersize", help="Size of Output layer.", type=int)
parser.add_argument("-wordstem", help="Nickname of word embeddings used.")
parser.add_argument("-dataf", help="Training data file.")
parser.add_argument("-samplingtype", help="Type of Sampling used.")
parser.add_argument("-peephole", help="Whether to use peepholes in LSTM.")
parser.add_argument("-outgate", help="Whether to use output gate in LSTM.")
parser.add_argument("-nonlinearity", help="Type of nonlinearity in projection model.",
                    type=int)
parser.add_argument("-nntype", help="Type of neural network.")
parser.add_argument("-regfile", help="Path to model file that we want to regularize towards.")

args = parser.parse_args()

params.batchsize = args.batchsize
params.hiddensize = args.hiddensize
params.wordfile = args.wordfile
params.nntype = args.nntype
params.layersize = args.layersize
params.wordstem = args.wordstem
params.dataf = args.dataf
params.type = args.samplingtype
params.peephole = str2bool(args.peephole)
params.outgate = str2bool(args.outgate)

regfile = args.regfile

if args.nonlinearity:
    if args.nonlinearity == 1:
        params.nonlinearity = lasagne.nonlinearities.linear
    if args.nonlinearity == 2:
        params.nonlinearity = lasagne.nonlinearities.tanh
    if args.nonlinearity == 3:
        params.nonlinearity = lasagne.nonlinearities.rectify
    if args.nonlinearity == 4:
        params.nonlinearity = lasagne.nonlinearities.sigmoid


(words, We) = getWordmap(params.wordfile)

examples = getData(params.dataf,words)

model = None

print sys.argv
#print params

if params.nntype == 'lstm':
    model = lstm_ppdb_model(params.layersize, params.peephole, params.outgate, regfile)

elif params.nntype == 'word':
    model = word_model(regfile)

elif params.nntype == 'proj':
    model = linear_projection_model(params.layersize, params.nonlinearity, regfile)
else:
    "Error no type specified"

model.train(examples, words, params)