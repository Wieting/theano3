import sys
import os

fname = sys.argv[1]
wordfile = sys.argv[2]
wordstem = sys.argv[3]
dim = 300
data = '../data/data-time-plot/ppdb-XL-ordered-data.txt'

LW = [1E-6,1E-7,1E-8]
batchsize = [100]
margins = [0.4]
samplingtypes = ['MAX','MIX']

cmd = "sh train_gouda.sh -wordstem {0} -wordfile {1} -outfile {2} \
-updatewords True -hiddensize 300 -dataf {3} -layersize {4} \
-save False -nntype word -evaluate True -epochs 5".format(wordstem,wordfile,fname,data,dim)

for i in batchsize:
    for j in LW:
        for k in margins:
            for l in samplingtypes:
                next_cmd = cmd + " -batchsize {0} -LW {1} -margin {2} -samplingtype {3}".format(i,j,k,l)
                print next_cmd
