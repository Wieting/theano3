from glob import glob

'''
0.368577960567 0.372433144712 FNWN2013 | 0.541110773226 0.54935316139 JHUppdb | 0.0877346038404 0.0940155174981 MSRpar2012 | 0.624911010054 0.640686062046 MSRvid2012 | 0.5
62138232792 0.556181190552 OnWN2012 | 0.512446563424 0.520632816212 OnWN2013 | 0.627239302046 0.654325151778 OnWN2014 | 0.490488210531 0.597201535128 SMT2012 | 0.374861558
775 0.358493550554 anno-dev | 0.432883639394 0.435585752183 anno-test | 0.583693752885 0.575511642968 bigram-jn | 0.332847954346 0.333972462691 bigram-nn | 0.506622356799
0.491938505432 bigram-vn | 0.407114872508 0.430466492815 deft-forum2014 | 0.419049536361 0.42515448872 deft-news2014 | 0.482986974594 0.4846402722 headline2013 | 0.4908897
29293 0.467484641814 headline2014 | 0.625452195662 0.594586338075 images2014 | 0.558331922848 0.516745360381 sicktest | 0.490594324977 0.457985588772 tweet-news2014 | 0.36
2772990491 0.316746636601 twitter |
'''

class dataset(object):
    def __init__(self, d):
        d = d.strip().split()
        self.name = d[-1]
        self.p = float(d[0])
	if len(d) == 3:
            self.s = float(d[1])

class evaluation(object):
    def __init__(self,line):
        self.result = line
        line = line.split("|")
        self.datasets = {}
        for i in line:
            if len(i.split()) > 0:
                d = dataset(i)
                self.datasets[d.name]=d

    def getTunedResults(self):
         return self.datasets['JHUppdb'].s

    def printResults(self):
        print self.result

def getScore(flist):
    dd = {}

    for j in flist:
        f=open(j,'r')
        lines = f.readlines()
        best=-1; keep=-1
        for i in lines:
            if 'command:' in i:
                name = i
            if 'JHUppdb' in i:
                e = evaluation(i)
                t = e.getTunedResults()
                if t > best:
                    best = t
                    dd[name] = (best,e.result)

    #sort dicts
    print len(dd)
    print len(flist)
    dd_sorted=sorted(dd.items(), key=lambda x: x[1][0], reverse=True)

    for i in dd_sorted:
        print i

flist = glob("training_phrases/SGE/sh.o*")
getScore(flist)
