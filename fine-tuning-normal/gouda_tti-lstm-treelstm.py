import sys
import os

fname = sys.argv[1]
wordfile = sys.argv[2]
wordstem = sys.argv[3]
dim = 300

LC = [1E-4,1E-5,1E-6]
dims = [50,100,150,200,250,300]
mems = [50,100,150,200,250,300]
batchsize = [25,50,100]

train = "../datasets_tokenized/sicktrain"
dev = "../datasets_tokenized/sickdev"
test = "../datasets_tokenized/sicktest"
minval = 1
maxval = 5

cmd = "sh train_gouda.sh -wordstem {0} -wordfile {1} -outfile {2} \
-updatewords False -hiddensize 300 -traindata {3} -devdata {4} \
-testdata {5} -save False -nntype lstm -evaluate True \
-epochs 20 \
-minval {6} -maxval {7}".format(wordstem,wordfile,
                                fname,train,dev,test,minval,maxval)

for i in LC:
    for j in mems:
        for k in dims:
            for l in batchsize:
                next_cmd = cmd + " -peephole False -outgate False -batchsize {3} -layersize {2} -memsize {1} -LC {0}".format(i,j,k,l)
                print next_cmd
                next_cmd = cmd + " -peephole False -outgate True -batchsize {3} -layersize {2} -memsize {1} -LC {0}".format(i,j,k,l)
                print next_cmd
                next_cmd = cmd + " -peephole True -outgate False -batchsize {3} -layersize {2} -memsize {1} -LC {0}".format(i,j,k,l)
                print next_cmd
                next_cmd = cmd + " -peephole True -outgate True -batchsize {3} -layersize {2} -memsize {1} -LC {0}".format(i,j,k,l)
                print next_cmd