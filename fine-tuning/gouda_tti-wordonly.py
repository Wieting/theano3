import sys
import os

fname = sys.argv[1]
wordfile = sys.argv[2]
wordstem = sys.argv[3]
dim = 300

LC = [1E-3,1E-4,1E-5,1E-6]
LW = [1E-1,1E-2,1E-3,1E-4,1E-5,1E-6,1E-7,1E-8]
batchsize = [25,50,100]
memsize = [50,150,300]

train = "../datasets_tokenized/sicktrain"
dev = "../datasets_tokenized/sickdev"
test = "../datasets_tokenized/sicktest"
minval = 1
maxval = 5

cmd = "sh train_gouda.sh -wordstem {0} -wordfile {1} -outfile {2} \
-updatewords True -hiddensize 300 -traindata {4} -devdata {5} \
-testdata {6} -layersize {3} -save False -nntype word -evaluate True \
-epochs 20 -minval {7} -maxval {8}".format(wordstem,wordfile,
                                                           fname,dim,train,dev,test,minval,maxval)

for i in batchsize:
    for j in LW:
        for k in LC:
            for l in memsize:
                next_cmd = cmd + " -batchsize {0} -LW {1} -LC {2} -memsize {3}".format(i,j,k,l)
                print next_cmd


'''
train = "../datasets_tokenized/twitter-train"
dev = "../datasets_tokenized/twitter-dev"
test = "../datasets_tokenized/twitter"
minval = 1
maxval = 2

cmd = "sh train_gouda.sh -wordstem {0} -wordfile {1} -outfile {2} \
-updatewords True -hiddensize 300 -traindata {4} -devdata {5} \
-testdata {6} -layersize {3} -save False -nntype word -evaluate True \
-epochs 20 -nonlinearity 1 -minval {7} -maxval {8}".format(wordstem,wordfile,
                                                           fname,dim,train,dev,test,minval,maxval)

for i in batchsize:
    for j in LW:
        for k in LC:
            for l in memsize:
                next_cmd = cmd + " -batchsize {0} -LW {1} -LC {2} -memsize {3}".format(i,j,k,l)
                #print next_cmd
'''