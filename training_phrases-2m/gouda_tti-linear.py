import sys
import os

fname = sys.argv[1]
wordfile = sys.argv[2]
wordstem = sys.argv[3]
dim = 300
#data = '../data/ppdb-XL-data.txt'
data = '../data/data-time-plot/2m_xxl_tuning.txt'

LC = [1E-4,1E-5,1E-6]
LW = [1E-6,1E-7,1E-8]
batchsize = [25,50,100]
margins = [0.4, 0.6, 0.8]
samplingtypes = ['MAX','MIX']

cmd = "sh train_gouda.sh -wordstem {0} -wordfile {1} -outfile {2} \
-hiddensize 300 -dataf {3} -layersize {4} \
-save False -nntype proj -evaluate True -epochs 5 -nonlinearity 1".format(wordstem,wordfile,fname,data,dim)

for i in batchsize:
    for k in margins:
        for l in samplingtypes:
            for m in LC:
                for j in LW:
                    next_cmd = cmd + " -updatewords True -batchsize {0} -LW {1} -margin {2} -samplingtype {3} -LC {4}".format(i,j,k,l,m)
                    print next_cmd
                next_cmd = cmd + " -updatewords False -batchsize {0} -margin {1} -samplingtype {2} -LC {3}".format(i,k,l,m)
                print next_cmd