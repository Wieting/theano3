best_word = "command: sh train_gouda.sh -wordstem simlex -wordfile /share/data/speech/Data/jwieting/paraphrase_embeddings/tuning/experiment2m/training_phrases-2m/../data/paragram_sl999.txt -outfile word-model -updatewords True -hiddensize 300 -dataf /share/data/speech/Data/jwieting/paraphrase_embeddings/tuning/experiment2m/training_phrases-2m/../data/data-time-plot/2m_xxl_tuning.txt -layersize 300 -save False -nntype word -evaluate True -epochs 5 -batchsize 100 -LW 1e-07 -margin 0.4 -samplingtype MIX"
best_proj = "command: sh train_gouda.sh -wordstem simlex -wordfile /share/data/speech/Data/jwieting/paraphrase_embeddings/tuning/experiment2m/training_phrases-2m/../data/paragram_sl999.txt -outfile proj-model -hiddensize 300 -dataf /share/data/speech/Data/jwieting/paraphrase_embeddings/tuning/experiment2m/training_phrases-2m/../data/data-time-plot/2m_xxl_tuning.txt -layersize 300 -save False -nntype proj -evaluate True -epochs 5 -nonlinearity 1 -updatewords True -batchsize 100 -LW 1e-07 -margin 0.4 -samplingtype MIX -LC 1e-06"
best_lstm = ""


cmd_word = "sh train_gpu.sh -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile word-model -updatewords True -hiddensize 300 -dataf ../data/data-time-plot/ppdb-XXL-ordered-data.txt -layersize 300 -save True -nntype word -evaluate True -epochs 10 -batchsize 100 -LW 1e-07 -margin 0.4 -samplingtype MIX"
cmd_proj = "sh train_gpu.sh -wordstem simlex -wordfile ../data/paragram_sl999.txt -outfile proj-model -hiddensize 300 -dataf ../data/data-time-plot/ppdb-XXL-ordered-data.txt -layersize 300 -save True -nntype proj -evaluate True -epochs 10 -nonlinearity 1 -updatewords True -batchsize 100 -LW 1e-07 -margin 0.4 -samplingtype MIX -LC 1e-06"

print cmd_word
print cmd_proj
